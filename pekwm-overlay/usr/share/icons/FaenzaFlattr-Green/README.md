### FaenzaFlattr-Green customized and fixed for KDE

![screen](http://i.imgur.com/fMyYvJs.png)

Icons are designed and developed by Uri Herrera <kaisergreymon99@gmail.com>

Many icons are designed and developed by Tago <tago73@gmail.com>

Merge Flattr with FaenzaFlattr-Green and fixed missing symlinks FadeMind <fademind@gmail.com>

[openDesktop.org Page](http://opendesktop.org/content/show.php?content=164770)

Icons are licensed under the [Creative Commons License (CC BY-NC-SA 4.0)](https://creativecommons.org/licenses/by-nc-sa/4.0/).
